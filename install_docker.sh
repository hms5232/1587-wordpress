#!/bin/bash


# Author: hms5232 ( https://gitlab.com/hms5232/1587-wordpress )
#
# 1. Docker
# 2. Docker Compose


# #########################################
# Install docker
# https://docs.docker.com/engine/install/ubuntu/

# uninstall old versions
sudo apt remove docker docker-engine docker.io containerd runc

# Set up the repository
sudo apt update
sudo apt install apt-transport-https ca-certificates curl gnupg-agent software-properties-common

# Add Docker’s official GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
# The following comand will show below info:
#
# pub   rsa4096 2017-02-22 [SCEA]
#       9DC8 5822 9FC7 DD38 854A  E2D8 8D81 803C 0EBF CD88
# uid           [ unknown] Docker Release (CE deb) <docker@docker.com>
# sub   rsa4096 2017-02-22 [S]
sudo apt-key fingerprint 0EBFCD88

# set up the stable repository
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

# Install Docker Engine
sudo apt update
sudo apt install docker-ce docker-ce-cli containerd.io

# hello docker
sudo docker run hello-world


# #########################################
# Instll Docker Compose
# https://docs.docker.com/compose/install/

sudo curl -L "https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version


# https://docs.docker.com/engine/install/ubuntu/#install-using-the-convenience-script
echo -e "If you would like to use Docker as a non-root user, you should now consider adding your user to the “docker” group with something like:\n"
echo -e "\t sudo usermod -aG docker <your-username> \n"
echo -e "\n"

