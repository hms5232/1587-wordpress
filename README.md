# 1587-wordpress

## 需求
* Linux 64 bit with apt package tool (Ubuntu is recommended)
* 網路連線能力

## 部署
### 前置作業
1. Docker & Dcoker Compose：請使用 `install_docker.sh` 或至官方文件依照教學安裝
2. 如果需要針對 docker compose 做一些非預設值的設定，可使用 `cp -i docker-compose-env.conf .env` 或直接 `vim .env` 來設定

### 首次部署（全新網站）
此部分適用於**全新網站**（沒有任何已存在之資料）
1. 執行 `before_wordpress.sh` 創建 WordPress 資料存放之資料夾並修改權限
2. `docker-compose up -d`

### 現有網站重新部署
此部分適合已經有網站資料，只是想快速重建者使用
1. 創建 `db_data` 資料夾並將網站資料庫檔案放入
2. 將資料（`wp-content`）複製至專案根目錄下
3. 執行 `before_wordpress.sh` 會修改資料夾權限
4. `docker-compose up -d`

## 參考來源
* [用 Docker Compose 快速生成一個 WordPress 網站（筆記，無細節說明）](https://qiita.com/vc7/items/e88026c75f2280f95ed4)
* [Volume mount when setting up Wordpress with docker](https://stackoverflow.com/questions/49202531/volume-mount-when-setting-up-wordpress-with-docker)
* [No write permission and no other selectable languages](https://github.com/docker-library/wordpress/issues/426#issuecomment-527581895)
