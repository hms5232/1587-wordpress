#!/bin/bash


# Author: hms5232 ( https://gitlab.com/hms5232/1587-wordpress )


# https://github.com/docker-library/wordpress/issues/426#issuecomment-527581895
# if wp-content not exist, just create it!
if [ ! -d "./wp-content" ] ; then
    mkdir -p wp-content/{plugins,themes}
    echo -e "create wp-content/ \n"
fi
# Change permission to www-data
sudo chown -R 33:33 wp-content
